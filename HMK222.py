import numpy as np
from matplotlib.path import Path
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import scipy.ndimage as ndimage
import cv2
fig = plt.figure()
import scipy.misc as misc
from glob import glob


"""
PART 1
"""

rate = 100
time = 60
nimage = 30

def GenerateRandomTriangle(x1,y1,x2,y2,x3,y3,sigma):
	# Generate Vertices

	y1 = 256 - y1
	y2 = 256 - y2
	y3 = 256 - y3

	cw_path  = Path([[y1,x1],[y2,x2],[y3,x3]])

	img = np.zeros((256,256))
	#im[2:-1,2:-2] = 1
	for i in range(0,256):
		for j in range(0,256):
			# im[i,j] = 1
			if cw_path.contains_point([i, j]):
				img[i,j] = 1

	blurry_img = ndimage.gaussian_filter(img,sigma)

	return blurry_img

def SpinAndShow(image, theta):

	 #img = ndimage.interpolation.rotate(image, theta)
	 img = ndimage.rotate(image, theta)
	 plt.imshow(img,cmap=plt.cm.gray) 

def animate(i):
	theta = time * rate
	dtheta = theta / nimage
	SpinAndShow(img, i * dtheta)

img = GenerateRandomTriangle(127,127+40,127-40,127-40,127+40,127-40,0)

# plt.imshow(img , cmap = plt.cm.gray)
# plt.axis('off')
# plt.savefig("triangle" + str(0) + ".png")
	
#ani = animation.FuncAnimation(fig, animate, init_func=None, frames=nimage, blit=False)
# ani.save('my_animation.mp4')
#plt.axis('off')
#plt.show()

"""
PART 2
"""
def binary_vertex_detector(img):

		img = cv2.imread('triangle0.png')
		gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
		kernel = np.ones((5,5), np.uint8)
		closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
		opening = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)
		vertex = closing - opening
		dilate = cv2.dilate(vertex, kernel)
		img[dilate<0.25]=[0,0,255]
		return img

plt.imshow(binary_vertex_detector(img),cmap = plt.cm.gray)
plt.savefig("binary_vertex_detector.png")
plt.show()

def harris_corner_detection(img):
		img = cv2.imread('triangle0.png')
		gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
		gray = np.float32(gray)
		dst = cv2.cornerHarris(gray,2,3,0.04)
		dst = cv2.dilate(dst,None)
		img[dst>0.2*dst.max()]=[0,0,255]
		return cv2.dilate(img,None)

# plt.imshow(harris_corner_detection(img),cmap = plt.cm.gray)
# plt.savefig("harris_corner_detector.png")
# plt.show()

"""
Part 3 - Optical Flow
"""
#Optical flow function (From OpenCV example)
def draw_flow(img, flow, step=16):
    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1)
    fx, fy = flow[y,x].T
    lines = np.vstack([x, y, x+fx, y+fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)
    vis = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(vis, lines, 0, (0, 255, 0))
    for (x1, y1), (x2, y2) in lines:
        cv2.circle(vis, (x1, y1), 1, (0, 255, 0), -1)
    return vis

def draw_hsv(flow):
    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]
    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2)
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    return bgr

def warp_flow(img, flow):
    h, w = flow.shape[:2]
    flow = -flow
    flow[:,:,0] += np.arange(w)
    flow[:,:,1] += np.arange(h)[:,np.newaxis]
    res = cv2.remap(img, flow, None, cv2.INTER_LINEAR)
    return res

def main():

    cap = cv2.VideoCapture('my_animation.mp4')
    
    ret, prev = cap.read()
    prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)
    show_hsv = False
    show_glitch = False
    cur_glitch = prev.copy()

    while True:
        ret, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(prevgray, gray, 0.5, 3, 15, 3, 5, 1.2, 0)
        prevgray = gray

        cv2.imshow('flow', draw_flow(gray, -flow))
        if show_hsv:
            cv2.imshow('flow HSV', draw_hsv(flow))
        if show_glitch:
            cur_glitch = warp_flow(cur_glitch, flow)
            cv2.imshow('glitch', cur_glitch)

        ch = 0xFF & cv2.waitKey(5)
        if ch == 27:
            break
        if ch == ord('1'):
            show_hsv = not show_hsv
            print 'HSV flow visualization is', ['off', 'on'][show_hsv]
        if ch == ord('2'):
            show_glitch = not show_glitch
            if show_glitch:
                cur_glitch = img.copy()
            print 'glitch is', ['off', 'on'][show_glitch]
    
    cv2.destroyAllWindows()
    
if __name__ == '__main__':
	main()




